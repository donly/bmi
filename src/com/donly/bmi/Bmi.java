package com.donly.bmi;

import java.text.DecimalFormat;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Bmi extends Activity {

	protected static final int MENU_ABOUT = Menu.FIRST;
	protected static final int MENU_EXIT = Menu.FIRST + 1;
	
	private Button calcbutton;
	private EditText fieldheight;
	private EditText fieldweight;
	private TextView viewSuggest;
	private TextView viewResult;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bmi);
		
		findViews();
	    setListeners(); 
	}

	private void findViews() {
	    calcbutton = (Button) findViewById(R.id.submit);
	    fieldheight = (EditText) findViewById(R.id.height);
	    fieldweight = (EditText) findViewById(R.id.weight);
	    viewResult = (TextView) findViewById(R.id.result);
	    viewSuggest = (TextView) findViewById(R.id.suggest);
	}
	
	// Listen for button clicks
	private void setListeners() {
	    calcbutton.setOnClickListener(calcBMI);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.bmi, menu);
		menu.add(0, MENU_ABOUT, 0, "关于");
		menu.add(0, MENU_EXIT, 0, "退出");
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		
		switch (item.getItemId()) {
		case MENU_ABOUT:
			openOptionsDialog();
			break;
		case MENU_EXIT:
			finish();
			break;
		default:
			break;
		}
		
		return true;
	}

	private Button.OnClickListener calcBMI = new Button.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			//Switch to report page
			Intent intent = new Intent();
			intent.setClass(Bmi.this, Report.class);
			startActivity(intent);
			
			if (fieldheight.getText().length() == 0) {
				Toast.makeText(Bmi.this, "请输入身高！", Toast.LENGTH_SHORT).show();
				return;
			} else if (fieldweight.getText().length() == 0) {
				Toast.makeText(Bmi.this, "请输入体重！", Toast.LENGTH_SHORT).show();
				return;
			}
			
			DecimalFormat format = new DecimalFormat("0.00");
			double height = Double.parseDouble(fieldheight.getText().toString()) / 100;
			double weight = Double.parseDouble(fieldweight.getText().toString());
			double BMI = weight / (height * height);
			
			// Present result
			viewResult.setText(getText(R.string.bmi_result) + format.format(BMI));
			
			// Give health advice
			if (BMI > 25) {
				viewSuggest.setText(R.string.advice_heavy);
			} else if (BMI < 20) {
				viewSuggest.setText(R.string.advice_light);
			} else {
				viewSuggest.setText(R.string.advice_average);
			}
		}
	};
	
	private void openOptionsDialog() {
		new AlertDialog.Builder(Bmi.this)
			.setTitle(R.string.about_title)
			.setMessage(R.string.about_message)
			.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
				
				}
			})
			.setNegativeButton(R.string.homepage_label, new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// go to url
					Uri uri = Uri.parse(getString(R.string.homepage_uri));
					Intent intent = new Intent(Intent.ACTION_VIEW, uri);
					startActivity(intent);
				}
			})
			.show();
		
//		Toast popup = Toast.makeText(Bmi.this, "BMI 计算器", Toast.LENGTH_SHORT);
//		popup.show();
	}
}
